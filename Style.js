import { StyleSheet } from "react-native";


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#CAEBFE00',
    },
    backgroundImageStyle: {
        height: "100%",
        width: "100%"
    },
    listStyle: {
        height: "100%",
        width: "100%"
    },
    listViewStyle: {
        width: "100%",
        height: 110,
        marginVertical: 7.5,
        alignItems: "center"
    },
    cardViewStyle: {
        height: "100%",
        width: "95%",
        justifyContent: "center",
        padding: 7.5
    },
    iconViewStyle: {
        height: 54,
        width: 54,
        borderRadius: 27,
        maxWidth: "20%",
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#F5CF00"
    },
    plusIconViewStyle: {
        height: 54,
        width: 54,
        borderRadius: 27,

        maxWidth: "20%",
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: "#F5CF00",
        opacity: 0.5
    },
    mainColStyle: {
        width: "72.5%",
        height: "75%",
        paddingLeft: 7.5,
        alignSelf: "center"
    },
    plusViewStyle: {
        width: "72.5%",
        height: "100%",
        justifyContent: 'center',
        paddingLeft: 7.5,
        paddingBottom: 7.5,
        opacity: 0.5
    },
    buttonStyle: {
        height: "100%",
        width: "100%"
    },
    titleTextStyle: {
        textAlign: "left",
        fontSize: 18
    },
    titleTaskTextStyle: {
        textAlign: "left",
        color: "#181743",
        fontSize: 18
    },
    descripTextSyle: {
        textAlign: "left",
        fontSize: 14,
        color: '#818181'
    },
    disCircleStyle: {
        height: 10,
        width: 10,
        borderRadius: 5,
        marginRight: 7.5,
        backgroundColor: '#5BAFDB'
    },
    pointTextStyle: {
        textAlign: "left",
        paddingLeft: 5,
        color: "#F5CF00",
        fontSize: 14
    },
    arrowViewStyle: {
        width: "7.5%",
        justifyContent: "center",
        alignItems: "center"
    },
    arrowIconStyle: {
        color: "#DDDDDD",
        fontSize: 32
    },
    itemIconStyle: {
        color: "#FFFFFF",
        fontSize: 30
    }
})