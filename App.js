import React, { Fragment } from 'react';
import { FlatList, Text, TouchableWithoutFeedback, ImageBackground } from 'react-native';
import { Container, ListItem, Card, Row, Col, Icon, View } from 'native-base';

import styles from "./Style";
console.disableYellowBox = true;

const Arr = [
  { 'icon': 'camera', 'title': 'Take out the rubbish', 'descrip': 'New Shoes', 'point': '+40' },
  { 'icon': 'camera', 'title': 'Do something cool!', 'descrip': 'New Shoes', 'point': '+40' },
  { 'icon': 'location', 'title': 'Prepare the Jennifer birthday', 'descrip': 'New Shoes', 'point': '+40' },
  { 'icon': 'watch', 'title': 'Make Dinner', 'descrip': 'New Shoes', 'point': '+40', 'time': 'Before 9am' },
  { 'icon': 'location', 'title': "Buy some gifts for the Jennifer's", 'descrip': 'New Shoes', 'point': '+40' },
  { 'icon': 'plus', 'title': 'New Smash Task' },
]
export default class App extends React.Component {

  renderItem = (param) => {
    return (
      <ListItem
        noBorder
        key={param.index}
        style={styles.listViewStyle}>
        <Card style={styles.cardViewStyle}>
          <TouchableWithoutFeedback style={styles.buttonStyle} onPress={() => alert("hello")}>
            <Row>
              <Card
                style={
                  param.item.icon === 'plus' ?
                    styles.plusIconViewStyle
                    :
                    styles.iconViewStyle
                }>
                {param.item.icon === 'camera' && <Icon style={styles.itemIconStyle} name={'ios-camera'} />}
                {param.item.icon === 'location' && <Icon style={styles.itemIconStyle} name={'ios-pin'} />}
                {param.item.icon === 'plus' && <Icon style={styles.itemIconStyle} name={'md-add'} />}
                {param.item.icon === 'watch' && <Icon style={styles.itemIconStyle} name={'md-time'} />}
              </Card>
              {param.item.icon === 'plus' ? (
                <Fragment>
                  <Col style={styles.plusViewStyle}>
                    <Text numberOfLines={1} style={styles.titleTaskTextStyle}>{param.item.title}</Text>
                  </Col>
                  <Col style={styles.arrowViewStyle}>
                    <Icon style={styles.arrowIconStyle} name={'ios-arrow-forward'} />
                  </Col>
                </Fragment>
              ) : (
                  <Fragment>
                    <Col style={styles.mainColStyle}>
                      <Text numberOfLines={1} style={styles.titleTextStyle}>{param.item.title}</Text>
                      <Row style={{ paddingVertical: 5, alignItems: 'center' }}>
                        <View style={styles.disCircleStyle} />
                        <Text style={styles.descripTextSyle}>{param.item.descrip}</Text>
                        <Text style={styles.pointTextStyle}>{param.item.point}</Text>
                        {param.item.time && (
                          <Row>
                            <Icon style={{ fontSize: 18, color: '#E0E0E0' }} name={'ios-time'} />
                            <Text style={{ fontSize: 14, color: '#818181' }}>{param.item.time}</Text>
                          </Row>
                        )}
                      </Row>
                    </Col>
                    <Col style={styles.arrowViewStyle}>
                      <Icon style={styles.arrowIconStyle} name={'ios-arrow-forward'} />
                    </Col>
                  </Fragment>
                )
              }
            </Row>
          </TouchableWithoutFeedback>
        </Card>
      </ListItem>
    )
  }

  render() {
    return (
      <Container style={styles.container}>
        <ImageBackground style={styles.backgroundImageStyle} source={{ uri: '' }}>
          <FlatList
            data={Arr}
            style={styles.listStyle}
            renderItem={(item, index) => this.renderItem(item, index)}
            keyExtractor={(item, index) => index}
          />
        </ImageBackground>
      </Container>
    );
  }
}